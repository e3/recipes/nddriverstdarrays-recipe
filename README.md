# NDDriverStdArrays conda recipe

Home: "https://github.com/areaDetector/NDDriverStdArrays"

Package license: EPICS Open License

Recipe license: BSD 3-Clause

Summary: EPICS NDDriverStdArrays module
